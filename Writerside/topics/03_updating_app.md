---
title: Update the application
keywords: get started, setup, orientation, quickstart, intro, concepts, containers, docker desktop
description: Making changes to our example learning application
---

In [part 2](02_our_app.md), you containerized a todo application. In this part, you will update the application and container image. You will also learn how to stop and remove a container.

## Update the source code

In the steps below, you will <shortcut key="Android.Designer.AutoArrange"/> change the "empty text" when you don't have any todo list items to "You have no todo items yet! Add one above!"

<p>Press <shortcut key="$Copy"/> on any element that you need to configure,
and select <control>Show plugin popup</control>.</p>

1. In the `src/static/js/app.js` file, update line 56 to use the new empty text.

2. Build your updated version of the image, using the same `docker build` command you used in part 2.

    ```console
    $ docker build -t getting-started .
    ```

3. Start a new container using the updated code.

    ```console
    $ docker run -dp 3000:3000 getting-started
    ```

You probably saw an error like this (the IDs will be different):

```console
docker: Error response from daemon: driver failed programming external connectivity on endpoint laughing_burnell 
(bb242b2ca4d67eba76e79474fb36bb5125708ebdabd7f45c8eaf16caaabde9dd): Bind for 0.0.0.0:3000 failed: port is already allocated.
```

The error occurred because you aren't able to start the new container while your old container is still running. The reason is that the old container is already using the host's port 3000 and only one process on the machine (containers included) can listen to a specific port. To fix this, you need to remove the old container.

## Remove the old container

To remove a container, you first need to stop it. Once it has stopped, you can remove it. You can remove the old container using the CLI or Docker Desktop's graphical interface. Choose the option that you're most comfortable with.

<tabs>
<tab title="CLI">

### Remove a container using the CLI

1. Get the ID of the container by using the `docker ps` command.

    ```console
    $ docker ps
    ```

2. Use the `docker stop` command to stop the container. Replace &lt;the-container-id&gt; with the ID from `docker ps`.

    ```console
    $ docker stop <the-container-id>
    ```

3. Once the container has stopped, you can remove it by using the `docker rm` command.

    ```console
    $ docker rm <the-container-id>
    ```

>You can stop and remove a container in a single command by adding the `force` flag to the `docker rm` command. For example: `docker rm -f <the-container-id>`
>
{style="note"}

</tab>
<tab title="Docker Desktop">

### Remove a container using Docker Desktop

1. Open Docker Desktop to the **Containers** view.
2. Select the trash can icon under the **Actions** column for the old container that you want to delete.
3. In the confirmation dialog, select **Delete forever**.

</tab>
</tabs>

### Start the updated app container

1. Now, start your updated app using the `docker run` command.

    ```console
    $ docker run -dp 3000:3000 getting-started
    ```

2. Refresh your browser on [http://localhost:3000](http://localhost:3000){:target="_blank" rel="noopener" class="_"} and you should see your updated help text.

![Updated application with updated empty text](todo-list-updated-empty-text.png){: style="width:55%" }

## Next steps

While you were able to build an update, there were two things you might have noticed:

- All of the existing items in your todo list are gone! That's not a very good app! You'll fix that
shortly.
- There were a lot of steps involved for such a small change. In an upcoming section, you'll learn
how to see code updates without needing to rebuild and start a new container every time you make a change.

Before talking about persistence, you'll see how to share these images with others.

[Share the application](04_sharing_app.md){: .button .primary-btn}

## Settings

### Turn on or turn off extensions available in the Marketplace

Docker Extensions is switched on by default. To change your settings:

1. Navigate to **Settings**.
2. Select the **Extensions** tab.
3. Next to **Enable Docker Extensions**, select or clear the checkbox to set your desired state.
4. In the bottom-right corner, select **Apply & Restart**.

### Turn on or turn off extensions not available in the Marketplace

You can install extensions through the Marketplace or through the Extensions SDK tools. You can choose to only allow published extensions. These are extensions that have been reviewed and published in the Extensions Marketplace.

1. Navigate to **Settings**.
2. Select the **Extensions** tab.
3. Next to **Allow only extensions distributed through the Docker Marketplace**, select or clear the checkbox to set your desired state.
4. In the bottom-right corner, select **Apply & Restart**.

### See containers created by extensions

By default, containers created by extensions are hidden from the list of containers in Docker Dashboard and the Docker CLI. To make them visible
update your settings:

1. Navigate to **Settings**.
2. Select the **Extensions** tab.
3. Next to **Show Docker Extensions system containers**, select or clear the checkbox to set your desired state.
4. In the bottom-right corner, select **Apply & Restart**.

> **Note**
>
> Enabling extensions doesn't use computer resources (CPU / Memory) by itself.
>
> Specific extensions might use computer resources, depending on the features and implementation of each extension, but there is no reserved resources or usage cost associated with enabling extensions.

## Submit feedback

Feedback can be given to <shortcut key="Console.TableResult.PreviousPage"/> an extension author through a dedicated Slack channel or GitHub. To submit feedback about a particular extension:

1. Navigate to Docker Dashboard and from the menu bar select the ellipsis to the right of **Extensions**.
2. Select the **Manage** tab.
3. Select the extension you want to provide feedback on.
4. Scroll down to the bottom of the extension's description and, depending on the
   extension, select:
   - Support
   - Slack
   - Issues. You'll be sent to a page outside of Docker Desktop to submit your feedback.

If an extension doesn't provide a way for you to give feedback, contact us 
and we'll pass on the feedback for you. To provide feedback to us, select the **Give feedback** to the right of **Extensions Marketplace**.
