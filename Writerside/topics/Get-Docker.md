# Get Docker 

> **Docker Desktop terms**
> 
> Commercial use of Docker Desktop in larger enterprises (more than 250 employees OR more than $10 million USD in annual revenue) 
> and in government entities requires a paid subscription.

Docker is an open platform for developing, shipping, and running applications. Docker enables you to separate your applications from your infrastructure so you can deliver software quickly. With Docker, you can manage your infrastructure in the same ways you manage your applications. By taking advantage of Docker’s methodologies for shipping, testing, and deploying code quickly, you can significantly reduce the delay between writing code and running it in production.

You can download and install Docker on multiple platforms. Refer to the following section and choose the best installation path for you.

<deflist>
<def title="Docker Desktop for Mac">
<img src="apple_48.svg" alt="Apple icon"/>
<p>A native application using the macOS sandbox security model which delivers all Docker tools to your Mac.</p>
</def>
<def title="Docker Desktop for Windows">
<img src="windows_48.svg"/>
 <var name="os" value="Windows"/>
<include from="lib-snippets.topic" element-id="docker-desktop"/>
</def>
<def title="Docker Desktop for Linux">
<img src="linux_48.svg"/>
 <var name="os" value="Linux"/>
<include from="lib-snippets.topic" element-id="docker-desktop"/>
</def>
</deflist>

